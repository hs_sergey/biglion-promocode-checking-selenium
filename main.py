# -*- coding: utf-8 -*-

from selenium import webdriver
import sys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.actions.pointer_actions import PointerActions

driver = webdriver.Chrome()
# browser.get('https://ssl.biglion.ru/checkout/order/puteshestvie-navozdushnom-share1-51/5144017/')
driver.get(sys.argv[1])

actions = PointerActions()
wait = WebDriverWait(driver, 10)
wait5 = WebDriverWait(driver, 5)


valid_promocodes = open("valid_promocodes.txt",'w')
invalid_promocodes = open("invalid_promocodes.txt",'w')

promocodes = [line.strip() for line in open('promocodes.txt', "r")]		
count = 0
for promocode in promocodes:
	count = count + 1
	print("processing promocode %s (%s from %s)" % (promocode, count, len(promocodes)))
	link = wait5.until(EC.visibility_of_element_located((By.CLASS_NAME, 'promocode_link')))
	driver.execute_script("arguments[0].scrollIntoView()", link)
	driver.execute_script("arguments[0].click();", link)
# 		link = driver.find_element_by_class_name('promocode_link')
# 		actions.click(link)
	editor = wait.until(EC.visibility_of_element_located((By.ID, 'new_promo')))
	editor.send_keys(promocode)
	editor.send_keys("\n")
	success = False
	try:
		element = wait5.until(EC.visibility_of_element_located((By.CLASS_NAME, 'code_activated')))
		success = True
	except Exception, e:
		pass
	if success:
		print("promocode %s is VALID" % promocode)
		valid_promocodes.write(promocode)
		valid_promocodes.write("\n")
		link = wait.until(EC.visibility_of_element_located((By.CLASS_NAME, 'cancel_pcode')))
		driver.execute_script("arguments[0].scrollIntoView()", link)
		driver.execute_script("arguments[0].click();", link)
	else:
		print("promocode %s is INVALID" % promocode)
		invalid_promocodes.write(promocode)
		invalid_promocodes.write("\n")
		link = wait.until(EC.visibility_of_element_located((By.CLASS_NAME, 'close')))
		driver.execute_script("arguments[0].scrollIntoView()", link)
		driver.execute_script("arguments[0].click();", link)
	driver.refresh()
valid_promocodes.close()
invalid_promocodes.close()			
print("all completed")
driver.quit()